#Programme adapt� par Jadin Guillaume et Flore �milie

Pour cette seconde mission nous avons du r�aliser, cette fois-ci en bin�me, un programme permettant de rechercher les racines d'une �quation diophantienne.
Le programme fonctionne de telle mani�re : 

On initialise une variable "solution", afin de garder en m�moire le nombre de solutions trouv�es par le programme, s'il y en a.
Vient apr�s une suite de saisie de valeurs (a, b et c) pour les exposants de nos inconnues, ainsi qu'une saisie visant � d�finir un maximum pour les trois inconnues.
On commence une m�ga-boucle, avec dans une boucle for le x mis en range, d�butant � 1, jusqu'au max saisi pr�c�dement (et augmentant, par d�faut, de 1 � chaque it�ration).
Dans cette boucle for du x, on en fait une autre pour y, et dans celle-ci, une autre pour z.
On place un if dans cette derni�re, qui nous permet de v�rifier si toutes les donn�es pour les inconnues, coupl�es avec les valeurs saisies pour les exposant, ont une solution en fonction de notre �quation diophantienne.
Si toutes ces donn�es donnent une solution, on l'affiche via un print, et on augmente la valeur de solution de 1, car 1 solution � �t� trouv�e.
On finit le programme par une condtion qui dit que si il y a eu des solutions il faut afficher combien il y en a eu, et s'il n'y en a pas eu, qu'il n'y en a pas eu.

Fin!