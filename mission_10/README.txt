#Réalisé par Guillaume Jadin
Lors de cette dixième mission, nous avons été chargé, en utilisant la programmation orientée objet, de créer des factures.

Fonctionnement du programme:

	- Il est divisé en plusieurs parties:

		- Le fichier "mission_10.py" qui contient toutes les objets et les classes du programme.

		- Le fichier "test.py" qui, comme son nom l'indique, test si les objets et classes du fichier "mission_10.py" fonctionnent.

	- Le fichier "mission_10.py":
 
	    Il contient les classes "Facture", "Article", "ArticleReparation", "Piece" et "ArticlePiece".

	- Le fichier "test.py":
		Il contient des classe qui testes les classes du fichier mission_10.py

Problèmes:

	- Nous avons eut quelques problèmes avec l'appel de la classe "Piece", cette incompréhension a ralenti quelque peu notre travail, notre programme n'est donc pas tout à fait achevé.