from mission10 import Facture
from mission10 import Article
from mission10 import ArticleReparation
from mission10 import Piece
from mission10 import ArticlePiece

"""
   Classe de test initiale pour la classe Facture.
   
   @author Kim Mens
   @version 18 novembre 2018
   (code adapté du code java de Charles Pecheur)
"""

class TestFactureInitial :

    articles = [ Article("laptop 15\" 8GB RAM", 743.79),
                 Article("installation windows", 66.11),
                 Article("installation wifi", 45.22),
                 Article("carte graphique", 119.49)
                 ]
    
    @classmethod
    def run(cls) :
        fac = Facture("PC Store - 22 novembre", cls.articles)
        print(fac)
        
#Réalisé par Guillaume Jadin
class TestFactureEtape1:
    
    articles = [ Article("laptop 15\" 8GB RAM", 743.79),
                 Article("installation windows", 66.11),
                 Article("installation wifi", 45.22),
                 Article("carte graphique", 119.49),
                 ArticleReparation(0.75),
                 ArticleReparation(1.75),
                 ArticleReparation(2.75)
                 ]
    
    @classmethod
    def run(cls) :
        fac = Facture("PC Store - 22 novembre", cls.articles)
        print(fac)
        
class TestFactureEtape3:
    articles = [ Article("laptop 15\" 8GB RAM", 743.79),
                 Article("installation windows", 66.11),
                 Article("installation wifi", 45.22),
                 Article("carte graphique", 119.49),
                 ArticlePiece(1, Article("disque dur 350 GB", 49.99)),
                 ArticlePiece(3, Article("souris bluetooth", 15.99)),
                 ArticleReparation(0.75),
                 ArticlePiece(5, Article("adaptateur DVI - VGA", 12.00)),
                 ArticlePiece(2, Article("Java in a Nutshell", 24.00), True),
                 ArticlePiece(5, Article("souris bluetooth", 15.99))
                 ]
    
    @classmethod
    def run(cls) :
        fac = Facture("PC Store - 22 novembre", cls.articles)
        print(fac)

class TestFactureEtape5:
    articles = [ Article("laptop 15\" 8GB RAM", 743.79),
                 Article("installation windows", 66.11),
                 Article("installation wifi", 45.22),
                 Article("carte graphique", 119.49),
                 ArticlePiece(1, Article("disque dur 350 GB", 49.99)),
                 ArticlePiece(3, Article("souris bluetooth", 15.99)),
                 ArticleReparation(0.75),
                 ArticlePiece(5, Article("adaptateur DVI - VGA", 12.00)),
                 ArticlePiece(2, Article("Java in a Nutshell", 24.00), True),
                 ArticlePiece(5, Article("souris bluetooth", 15.99))
                 ]
    livraison = [ Piece("disque dur 350 GB", 49.99, 0.355, 1, True),
                  Piece("souris bluetooth", 15.99, 0.176, 3),
                  Piece("adaptateur DVI - VGA", 12.00, 0.000, 5),
                  Piece("Java in a Nutshell", 24.00, 0.321, 2, False, True),
                  Piece("souris bluetooth", 15.99, 0.176, 5)
                 ]
    
    @classmethod
    def run(cls) :
        fac = Facture("PC Store - 22 novembre", cls.articles)
        liv = Facture("PC Store - 22 novembre", cls.livraison)
        liv2 = liv.livraison_str()
        print(fac)
        print(liv2)

if __name__ == "__main__":
    #TestFactureInitial.run()
    #TestFactureEtape1.run()
    #TestFactureEtape3.run()
    TestFactureEtape5.run()
