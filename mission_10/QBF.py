class Item:
    
    def __init__(self, author, title, serial):
        self.__author = author
        self.__title = title
        self.__serial = serial
        
    def __str__(self):
        return "[{}] {}, {}".format(self.__serial, self.__author, self.__title)
        
class CD(Item):
    
    __serial = 100000
    
    def __init__(self, author, title, duree):
        super().__init__(author, title, self.__serial)
        CD.__serial += 1
        self.__duree = duree
        
        
        
    def __str__(self):
        return "{} ({} s)".format(super().__str__(), self.__duree)
        
        
        
a = CD("Radiohead", "The Bends", 2917)
b = CD("Radiohead", "The Bends", 2917)
print(a)
print(b)