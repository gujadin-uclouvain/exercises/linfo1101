"""
    Une facture, sous forme d'une liste d'articles.
   
    @author Kim Mens
    @version 18 novembre 2018
    (code adapté du code java de Charles Pecheur)
"""

class Facture :
    
    __next_num = 1

    def __init__(self, description, articles):
        """
        Crée une facture avec une description (titre) et un liste d'articles.
        """
        self.__reference = description
        self.__articles = articles

    def description(self):
        """
        Retourne la description de cette facture.
        """
        return self.__reference

    def articles(self):
        """
        Retourne la liste des articles de cette facture.
        """
        return self.__articles
        
    def __str__(self):
        """
        Retourne la représentation string d'une facture, à imprimer avec la méthode print().
        """
        s = self.entete_str()
        totalPrix = 0.0
        totalTVA = 0.0
        for art in self.articles() :
            s += self.article_str(art)
            totalPrix = totalPrix + art.prix()
            totalTVA = totalTVA + art.tva()
        s += self.totaux_str(totalPrix, totalTVA)
        return s

    def entete_str(self):
        """
        Imprime l'entête de la facture, comprenant le descriptif et les têtes de colonnes.
        """
        e = "Facture No {} : ".format(self.__next_num)
        self.__next_num += 1
        e += "Facture " + self.description() + "\n"
        e += self.barre_str()
        e += "| {0:<40} | {1:>10} | {2:>10} | {3:>10} |\n".format("Description","prix HTVA","TVA","prix TVAC")
        e += self.barre_str()
        return e
    
    def entete_str_livraison(self):
        """
        Imprime l'entête de la facture, comprenant le descriptif et les têtes de colonnes.
        """
        e = "Livraison - Facture No {} : ".format(self.__next_num)
        e += "Facture " + self.description() + "\n"
        e += self.barre_str()
        e += "| {0:<40} | {1:>10} | {2:>10} | {3:>10} |\n".format("Description","poids/pce","nombre","poids")
        e += self.barre_str()
        return e

    def barre_str(self):
        """
        Retourne un string représentant une barre horizontale sur la largeur de la facture.
        """
        b = ""
        barre_longeur = 83
        for i in range(barre_longeur):
            b += "="
        return b + "\n"

    def article_str(self, art):
        """
        Retourne un string correspondant à une ligne de facture pour l'article art
        """
        return "| {0:40} | {1:10.2f} | {2:10.2f} | {3:10.2f} |\n".format(art.description(), art.prix(), art.tva(), art.prix_tvac())
    
    def totaux_str(self, prix, tva):
        """
        Retourne un string représentant une ligne de facture avec les totaux prix et tva, à imprimer en bas de la facture
        """
        b = self.barre_str()
        b += "| {0:40} | {1:10.2f} | {2:10.2f} | {3:10.2f} |\n".format("T O T A L", prix, tva, prix+tva)
        b += self.barre_str()
        return b
        
    # This method needs to be added during Etape 4 of the mission
    def nombre(self, pce) :
        """
        Retourne le nombre d'exemplaires de la pièce pce dans la facture, en totalisant sur tous les articles qui concernent cette pièce.
        """
        compt = 0
        #for art in self.articles():
            #compt += 1
        return compt

    # This method needs to be added during Etape 5 of the mission
    def livraison_str(self):
        """
        Cette méthode est une méthode auxiliaire pour la méthode printLivraison
        """
        s = self.entete_str_livraison()
        for art in self.articles() :
            s += "| {0:40} | {1:8.2f}kg | {2:10.2f} | {3:8.2f}kg |\n".format(art.description(), art.poids(), art.nombre(), art.poids()*art.nombre())
        return s
        
        


"""
    Un article de facture simple, comprenant un descriptif et un prix.
   
    @author Kim Mens
    @version 18 novembre 2018
    (code adapté du code java de Charles Pecheur)
"""
 
class Article :

    __taux_tva = 0.21   # TVA a 21%
    
    def __init__(self,d,p):
        """
        Cree un article avec description d et prix p.
        """
        self.__description = d
        self.__prix = p

    def description(self):
        """
        Retourne la description de cet article.
        """
        return self.__description
        
    def prix(self):
        """
        Retourne le prix (HTVA) de cet article.
        """
        return self.__prix
        
    def taux_tva(self):
        """
        Retourne le taux de TVA (même valeur pour chaque article)
        """    
        return self.__taux_tva

    def tva(self):
        """
        Retourne la TVA sur cet article
        """    
        return self.prix() * self.taux_tva()
 
    def prix_tvac(self):
        """
        Retourne le prix de l'article, TVA compris.
        """
        return self.prix() + self.tva()

    def __str__(self):
        """
        Retourne un texte decrivant cet article, au format: "{description}: {prix}"
        """
        return "{0}: {1:.2f}".format(self.get_description, self.get_prix())

#----------------------------------------------------------------------------------------------------#
#Réalisé par Guillaume Jadin
class ArticleReparation(Article):
    
    def __init__(self, duree):
        self.__duree = float(duree)
        
    def description(self):
        return "Reparation ({} heures)".format(self.__duree)
    
    def prix(self):
        fixe = 20
        var = 35
        return 20 + 35*self.__duree
    
class Piece():
    
    def __init__(self, d, p, poids = 0.0, nbr = 1, fragile = False, tva_reduit = False):
        self.__description = d
        self.__prix = p
        self.__poids = poids
        self.__nombre = nbr
        self.__fragile = fragile
        self.__tva_reduit = tva_reduit
        
    def description(self):
        return self.__description
    
    def prix(self):
        return self.__prix
    
    def nombre(self):
        return self.__nombre
    
    def poids(self):
        return self.__poids
    
    def fragile(self):
        return self.__fragile
    
    def tva_reduit(self):
        return self.__tva_reduit
    
    def __eq__(self, other):
        return (self.__description == other.__description) and (self.__prix == other.__prix)
    
class ArticlePiece(Article):
    
    def __init__(self, nbr, piece, if_tva_reduit = False):
        self.__piece = piece.description()
        self.__prix = piece.prix()
        self.__nbr = nbr
        self.__if_tva_reduit = if_tva_reduit
        
    def piece(self):
        return self.__piece
    
    def nombre(self):
        return self.__nbr
    
    def tva_reduit(self):
        return self.__if_tva_reduit
    
    def description(self):
        return "{} * {} @ {}".format(self.nombre(), self.piece(), self.__prix)
    
    def prix(self):
        return self.__prix * self.nombre()
    
    def tva(self):
        if self.tva_reduit():
            return self.__prix * 0.06
        else:
            return super().tva()