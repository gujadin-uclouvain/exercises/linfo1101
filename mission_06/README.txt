Lors de cette sixième mission, nous avons été chargé de créer un outil.

Fonctionnement du programme:

	- Il est divisé en deux parties:

		- La première est le fichier "assistant.py" qui contient toutes les fonctions du programme.

		- La seconde est le fichier "test.py" qui, comme son nom l'indique, test si les fonctions du fichier "assistant.py" fonctionnent.

	- Le fichier "assistant.py":
 
	    - file <name>: spécifie le nom d'un fichier sur lequel l'outil doit travailler
	    
	    - info: montre le nombre de lignes et de caractères du fichier
	    
	    - dictionary: utilise le fichier comme dictionnaire à partir de maintenant
	    
	    - search <word>: cherche le mot le plus similaire au mot spécifié dans le dictionnaire
	    
	    - sum <number1> ... <numbern>: calcule la somme des nombres spécifiés
	    
	    - avg <number1> ... <numbern>: calcule la moyenne des nombres spécifiés
	    
	    - help: montre des instructions à l'utilisateur
	    
	    - exit: arrête l'outil



Problèmes:

	- Nous n'avons malheureusement pas réussi à faire fonctionner la fonction search. Celle-ci retourne une erreur d'indexation.
