import sys
##Functions - START
def line_count(filename):
    with open(filename, "r") as file:
        data = file.readlines()
        count = 0
        for line in data:
            count += 1
    return count
    
def char_count(filename):
    with open(filename, "r") as file:
        all_lines = file.read()
        all_char = len(all_lines) 
    return all_char

def search(dictionary, word):
    try:
        first = 0
        last = len(dictionary)-1
        found = []
        lst = []
        for line in dictionary:
            for m in range(len(line)):
                if line[m] == ",":
                    l = line.split(",")
                    if type(l[0]) == str:
                        lst.append(l[0])

        while first<=last and not found:
            middle = (first + last)//2
            if llst[middle] == word:
                found = lst[middle]
            else:
                if word < lst[middle]:
                    last = middle-1
                else:
                    first = middle+1
        
        if found != []:
            return found
        else:
            print("No solution found")
        
    except NameError:
        print("No file and dictionary opened")
##Functions - END
Tools = True
while Tools == True:
    command = input("Enter your command: ").lower()
    parameters = command.split ()
    try:
        if parameters[0] == "exit":
            try:
                file.close()
                sys.exit("Goodbye")
            except NameError:
                sys.exit("Goodbye")
            
        elif parameters[0] == "help":
            print("""\n\tfile <name>: spécifie le nom d'un fichier sur lequel l'outil doit travailler,
    \n\tinfo: montre le nombre de lignes et de caractères du fichier,
    \n\tdictionary: utilise le fichier comme dictionnaire à partir de maintenant,
    \n\tsearch <word>: cherche le mot le plus similaire au mot spécifié dans le dictionnaire,
    \n\tsum <number1> ... <numbern>: calcule la somme des nombres spécifiés,
    \n\tavg <number1> ... <numbern>: calcule la moyenne des nombres spécifiés,
    \n\thelp: montre des instructions à l'utilisateur,
    \n\texit: arrête l'outil\n""")

            
        elif parameters[0] == "file":
            try:
                file = open(parameters[1])
                filename = parameters[1]
                print("Loaded {0}".format(filename))
            except IndexError:
                print("Enter a filename after 'file'")
            except FileNotFoundError:
                print("No such file or directory: {0}".format(parameters[1]))
            
        elif parameters[0] == "info":
            try:
                print("{0} lines".format(line_count(filename)))
                print("{0} caracters".format(char_count(filename)))
            except NameError:
                print("No file opened")
                
        elif parameters[0] == "dictionary":
            try:
                with open(filename, "r") as file:
                    dictionary = file.readlines()
                    dictionary.sort()
                print("Read file as dictionary")
            except NameError:
                print("No file opened")
        
        elif parameters[0] == "search":
            try:
                word = parameters[1]
                print("Closest word is {0}".format(search(dictionary, word)))
                
            except IndexError:
                print("Enter a word to search after 'search'")
                
        elif parameters[0] == "sum":
            try:
                sol = 0
                for n in range(1, len(parameters)):
                    sol += float(parameters[n])
                print(sol)
            except IndexError:
                print("Enter numbers after 'sum'")
            except ValueError:
                print("Enter only numbers after 'sum'")
                
        elif parameters[0] == "avg":
            try:
                sol = 0
                for n in range(1, len(parameters)):
                    sol += float(parameters[n])
                sol = sol/(len(parameters)-1)
                print(sol)
            except IndexError:
                print("Enter numbers after 'avg'")
            except ValueError:
                print("Enter only numbers after 'avg'")
                
    except IndexError:
        print("Insert a command")

