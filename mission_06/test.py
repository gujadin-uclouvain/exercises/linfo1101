import assistant

def test(answer):
    import sys
    ligne = sys._getframe(1).f_lineno
    if answer:
        msg = "Test de la ligne {0} => OK.".format(ligne)
    else:
        msg = ("Test de la ligne {0} => ERREUR.".format(ligne))
    print(msg)

def test_line_count():
    test(assistant.line_count(error1.dat) == 6)
    test(assistant.line_count(error2.dat) == 6)
    
test_line_count()
