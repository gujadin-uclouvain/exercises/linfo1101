def table(filename_in, filename_out, width):
    result = ""
    entete = "+-{}-+".format("-"*width)
    result += entete + "\n"
    with open(filename_in, "r") as file_in:
        for line in file_in:
            line = line.strip()
            line = list(line)
            result += "| "
            entry = ""
            for g in range(len(line)):
                if len(entry) < width:
                    entry += line[g]
                    smaller_than_width = True
                else:
                    smaller_than_width = False
            
            if smaller_than_width:
                space_nbr = width - len(line)
                entry += " "*space_nbr
            entry += " |\n"
            result += entry
            
        result += entete
        
    with open(filename_out, "w") as file_out:
        file_out.write(result)
    

table("filename_in.txt", "filename_out.txt", 8)