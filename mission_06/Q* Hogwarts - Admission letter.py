def write(letter_template, name):
    result = "ECOLE de MAGIE et de SORCELLERIE de POUDLARD\n"
    result += "Directeur : Albus Dumbledore\n"
    result += "(Ordre de Merlin, Première classe, Grand Sorc., Chf. Démoniste, Suprême Mugwump, Confédéré international. des sorciers)\n\n"
    result += "Cher Cher M./Mme "
    with open (letter_template, "w") as file:
        result += "{},\n\n".format(name)
        result +="Nous avons le plaisir de vous informer que vous avez été accepté à l'Ecole de Magie et de Sorcellerie de Poudlard. Veuillez trouver ci-joint une liste de tous les livres et équipements nécessaires.\n\n"
        result += "Les cours débutent le 1er septembre. Nous attendons votre chouette pour le 31 juillet au plus tard.\n\n"
        result += "Cordialement,\n\n"
        result += "Minerva McGonagall\n"
        result += "Directrice Adjointe"
        file.write(name)
