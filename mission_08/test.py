#Alexandre Verlaine & Guillaume Jadin
import memoisation as mem
import time
if __name__ == "__main__":
    def test(answer):
        import sys
        ligne = sys._getframe(1).f_lineno
        if answer:
            msg = "Test de la ligne {0} => OK.".format(ligne)
        else:
            msg = ("Test de la ligne {0} => ERREUR.".format(ligne))
        print(msg)      
        
    def test_fibo_mem():
        lst = [100, 200, 300, 400, 500, 600]
        for n in lst:
            t1 = time.clock()
            f = mem.fibo_mem(n)
            t2 = time.clock()
            d = t2 - t1
            print("fibo_mem({0}) = {1} ({2:.2f} sec)".format(n, f, d))

    test_fibo_mem()    
