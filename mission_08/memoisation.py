#Alexandre Verlaine & Guillaume Jadin
import time
import turtle

#ETAPE 1:
def fibo(n):
    if n <= 1:
        return n
    t = fibo(n-1) + fibo(n-2)
    return t

def fibonacci(n):
    lst = [0, 1]
    for g in range(n-1):
        h = g+1
        lst.append(lst[g]+lst[h])
    return lst[n]
    
def comparaison():
    n = 37
    t1_re = time.clock()
    f = fibo(n)
    t2_re = time.clock()
    t1_it = time.clock()
    f = fibonacci(n)
    t2_it = time.clock()
    d_it = t2_it - t1_it
    d_re = t2_re - t1_re
    print("fibo({0}) = {1} ({2:.2f} sec)".format(n, f, d_re))
    print("fibonacci({0}) = {1} ({2:.2f} sec)".format(n, f, d_it))

#ETAPE 2:
dict_mapping = {0: 0, 1: 1}
def fibo_mem(n):
    if n not in dict_mapping:
        t = fibo_mem(n-1) + fibo_mem(n-2)
        dict_mapping[n] = t
    return dict_mapping[n]
    
#ETAPE 3:
def calcul_fibo(g):
    l=[]
    while g != 0:
        l.append(fibo(g))
        g -= 1 
    l.sort()
    return l

def draw_bar(t, height):
    t.begin_fill()
    t.left(90)
    t.forward(height)
    t.write("  "+ str(height))
    t.right(90)
    t.forward(40)
    t.right(90)
    t.forward(height)
    t.left(90)
    t.end_fill()
    t.forward(10)

def histogramme(f, x):
    wn = turtle.Screen()
    wn.bgcolor("lightgreen")

    tess = turtle.Turtle()
    tess.color("blue", "red")
    tess.pensize(3)

    xs = f(x)

    for a in xs:
        draw_bar(tess, a)

    wn.mainloop()
    
def hist_fibo():
    return histogramme(calcul_fibo, 10)

#hist_fibo()

#ETAPE 4:

    
#ETAPE 5:
def exec_time_old(f, n):
    t1 = time.clock()
    fct = f(n)
    t2 = time.clock()
    d = t2 - t1
    return d

def exec_time(f):
    t1 = time.clock()
    fct = f(lambda n: n)
    t2 = time.clock()
    d = t2 - t1

def histogramme_exec(lst):
    wn = turtle.Screen()
    wn.bgcolor("lightgreen")

    tess = turtle.Turtle()
    tess.color("blue", "red")
    tess.pensize(3)

    for a in lst:
        draw_bar(tess, a)

    wn.mainloop()
    
def hist_exec_time(g):
    lst = []
    for n in range(g, g+11):
        lst.append(exec_time_old(fibo, n))
    return histogramme_exec(lst)
    
#hist_exec_time(20)