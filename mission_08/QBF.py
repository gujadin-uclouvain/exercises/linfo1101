#Function in a function:
def fib(n):
    memo = {0: 0, 1: 1}
    
    def fib_mem(x):
        if x not in memo:
            new_value = fib_mem(x-1) + fib_mem(x-2)
            memo[x] = new_value
        return memo[x]

    return fib_mem(n)

#Ordre sup + memoisation
dict = {f_fib: {0: 0, 1: 1}, f_fact: {}}

def memo(fun, n):
    if n not in dict[fun]:
        dict[fun][n] = fun(n)
    return dict[fun][n]