#Retourner nbr d'occurence:
def fib(n):
    if n <= 1:
        return 1
    a = fib(n-1) + fib(n-2)
    return a + 1

print(fib(4))