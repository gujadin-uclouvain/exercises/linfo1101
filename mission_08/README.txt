Lors de cette huitième mission, nous avons été chargé de, grâce à la mise en place de nombreuses fonctions, déterminer une réflexion sur les concepts de récursion, d'itération et de mémoïsation.

Fonctionnement du programme:

	- Il est divisé en plusieurs parties:

		- Le fichier "memoisation.py" qui contient toutes les fonctions du programme.

		- Le fichier "test.py" qui, comme son nom l'indique, test si les fonctions du fichier "memoisation.py" fonctionnent.

		- Le rapport sur les concepts en pdf.

	- Le fichier "memoisation.py":
 
	    Il contient les fonctions des étapes 1 à 5 (sauf la 4ème)

Problèmes:

	- Les exercices abordant les fonctions d'ordres supérieurs (4 et 5) ont été difficile à mettre / n'ont pas pu être mis en place à cause d'une incompréhension de ce point de matière.