class Employe:
    
    def __init__(self, n, s):
        self.name = n
        self.salaire = s
        
    def __str__(self):
        return "{0} : {1}".format(self.name, self.salaire)
    
    def print_name(self):
        return self.name
    
    def print_salaire(self):
        return self.salaire
    
    def augmente(self, nbr):
        pourcent = (nbr/100) * self.salaire
        self.salaire += pourcent
        return self
    
    #def addlastname(self, lastname):
        #self.name += " " + lastname
        #return self
    
charles = Employe("Charles", 2500)
charles.augmente(10)
print(charles)
#charles.addlastname("Pecheur")
#print(charles)
#print(charles.print_name())
#print(Employe("Kim", 2400).augmente(10).addlastname("Mens"))
