#Créé par Guillaume Jadin et Emilie Flore
import mission_9 as mi

##TESTS CLASSE DUREE:
print(("-"*70 + "\nTESTS CLASSE {0}:\n" + "-"*70).format("DUREE"))
#__init__
o = mi.Duree(1,124,74)
print(("FUNCTION {0} => {1}").format("__init__", o))

#toSecondes(self)
d = mi.Duree(0,25,45)
print(("FUNCTION {0} => {1}").format("toSecondes", d.toSecondes()))

#delta(self, d)
x = mi.Duree(0,18,17)
delt = d.delta(x)
print(("FUNCTION {0} => {1}").format("delta", delt))

#apres(self, d)
apr = d.apres(x)
print(("FUNCTION {0} => {1}").format("apres", apr))

#ajouter(self, d)
aj = d.ajouter(x)
print(("FUNCTION {0} => {1}").format("ajouter", aj))


##TESTS CLASSE CHANSON:
print("\n")
print(("-"*70 + "\nTESTS CLASSE {0}:\n" + "-"*70).format("CHANSON"))
#__init__
ch = mi.Chanson("Love_Theme_From_'The Godfather'", "Nino_Rota", mi.Duree(0,2,39))
print(("FUNCTION {0} => {1}").format("__init__", ch))


##TESTS CLASSE ALBUM:
print("\n")
print(("-"*70 + "\nTESTS CLASSE {0}:\n" + "-"*70).format("ALBUM"))
#__init__
a = mi.Album("The Godfather (Soundtrack)")
print(a)

#add(self, chanson)
ad = a.add("Apollonia - Nino_Rota - 00:01:21")
print(("\nFUNCTION {0} => {1}\n").format("add", ad))
print(a)
ad = a.add("The_New_Godfather - Nino_Rota - 00:01:59")
print(("\nFUNCTION {0} => {1}\n").format("add", ad))
print(a)

