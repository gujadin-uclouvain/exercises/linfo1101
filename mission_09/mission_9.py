#Créé par Guillaume Jadin
class Duree:
    def __init__(self, h, m, s):
        #while not s < 60:
        #    s -= 60
        #    m += 1
        #while not m < 60:
        #    m -= 60
        #    h += 1
        m += s//60
        s = s%60
        
        h += m//60
        m = m%60
            
        self.heure = h
        self.min = m
        self.sec = s
        
    def __str__(self):
        """
        Retourne cette durée sous la forme de texte "heures:minutes:secondes".
        Astuce: la méthode "{:02}:{:02}:{:02}".format(heures, minutes, secondes)
        retourne le String désiré avec les nombres en deux chiffres en ajoutant
        les zéros nécessaires.
        """
        return "{0:02}:{1:02}:{2:02}".format(self.heure, self.min, self.sec)
    
    def toSecondes(self):
        """
        Retourne le nombre total de secondes de cette instance de Duree (self).
        """
        while self.heure != 0:
            self.heure -= 1
            self.min += 60
            
        while self.min != 0:
            self.min -= 1
            self.sec += 60
            
        return self.sec
    
    def delta(self, d):
        """
        Retourne la différence entre cette instance de Duree (self) et la Duree d passée en paramètre,
        en secondes (positif si ce temps-ci est plus grand).
        """
        d1 = Duree.toSecondes(self)
        d2 = Duree.toSecondes(d)
        return d1 - d2
    
    def apres(self, d):
        """
        Retourne True si cette instance de Duree (self) est plus grand que la Duree d passée en paramètre;
        retourne False sinon.
        """
        d1 = Duree.toSecondes(self)
        d2 = Duree.toSecondes(d)
        if d1 >= d2:
            return True
        else:
            return False
        
    def ajouter(self, d):
        """
        Ajoute une autre Duree d à cette instance de Duree (self).
        Corrige de manière à ce que les minutes et les secondes soient dans l'intervalle [0..60[,
        en reportant au besoin les valeurs hors limites sur les unités supérieures
        (60 secondes = 1 minute, 60 minutes = 1 heure).
        """
        d = Duree.toSecondes(d)
        self.sec += d
        while not self.sec < 60:
            self.sec -= 60
            self.min += 1
        while not self.min < 60:
            self.min -= 60
            self.heure += 1
            
        return self


class Chanson:
    def __init__(self, t, a, d):
        self.title = t
        self.artist = a
        self.duree = d
        
    def __str__(self):
        """
        Retourne un String décrivant cette chanson sous le format "TITRE - AUTEUR - DUREE".
        Par exemple: "Let's_Dance - David_Bowie - 00:04:05"
        """
        return "{0} - {1} - {2}".format(self.title, self.artist, self.duree)


class Album:
    def __init__(self, n):
        self.name_album = n
        self.lst_album = []
        self.duree_tot = Duree(0, 0, 0)
        
    def __str__(self):
        album = "{0} ({1} chansons, {2})".format(self.name_album, len(self.lst_album), self.duree_tot)
        
        if self.lst_album == []:
            return album
        else:
            for g in range(len(self.lst_album)):
                album += "\n{0:02}: {1}".format(g+1, self.lst_album[g])
        return album
        
    def add(self, chanson):
        """
        Ajoute une chanson à un album.
        Cette méthode retourne False si lors de l'ajout d'une chanson l'album a atteint 100 chansons ou la durée dépasserait 75 minutes.
        Sinon la chanson est rajoutée et la méthode add retourne True.
        """
        self.lst_album.append(str(chanson))
        seconds = 0
        minutes = 0
        hours = 0
        for h in range(len(self.lst_album)):
            seconds += int(self.lst_album[h][-2:])
            minutes += int(self.lst_album[h][-5:-3])
            hours += int(self.lst_album[h][-8:-6])
        duree_tot = Duree(hours, minutes, seconds)
        
        if len(self.lst_album) > 100 or duree_tot.toSecondes() > 4500:
            del self.lst_album[-1]
            return False
        else:
            self.duree_tot = Duree(hours, minutes, seconds)
            return True


if __name__ == "__main__":
    with open("music-db.txt", "r") as file:
        not_change_album = False
        compt = 0
        for count_line, line in enumerate(file):
            debugger_album = True
            while debugger_album == True:
                if type(line) == list:
                    pass
                else:
                    line = line.split()
                d = Duree(0, int(line[2]), int(line[3]))
                song = Chanson(line[0], line[1], d)
                
                if not_change_album == False:
                    compt += 1 
                    album = Album("Album {0}".format(compt))
                    not_change_album = True
                
                not_change_album = album.add(song)
                debugger_album = False
                if not_change_album == False:
                    debugger_album = True
                    print()
                    print(album)
        print()
        print(album)
