Lors de cette neuvième mission, nous avons été chargé, en utilisant la programmation orientée objet, de charger un fichier contenant des musiques et de les classer par albums.

Fonctionnement du programme:

	- Il est divisé en plusieurs parties:

		- Le fichier "mission_9.py" qui contient toutes les objets et les classes du programme.

		- Le fichier "test.py" qui, comme son nom l'indique, test si les objets et classes du fichier "mission_9.py" fonctionnent.

	- Le fichier "mission_9.py":
 
	    Il contient les classes "Duree", "Chanson" et "Album". 
	    Il contient aussi une série d'instructions pour afficher et trier les chansons du fichier "music-db.txt".

Problèmes:

	- Aucun problème n'a été rencontré lors de la réalisation de cette mission. 