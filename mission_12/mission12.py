from PIL import Image
import os, sys

class ImageFolder():
    
    def __init__(self, path):
        self.__path = path
        
        self.__dir_lst = [1, 1, 1]
        
    def path(self):
        return self.__path
    
    def next (self, name):
        def file_instance(folder):
            compt = 0
            try:
                with os.scandir(self.path() + "/{}/".format(folder)) as it:
                    for entry in it:
                        if not entry.name.startswith('.') and entry.is_file():
                            compt += 1
                return compt
            except:
                raise FileNotFoundError("Le chemin entré n'existe pas ou ne fait pas référence aux bon dossiers d'images...")
        
        if name != "1" and name != "2" and name != "3":
            return "None"
        
        name = int(name)
        if name > len(self.__dir_lst):
            raise IndexError ("le nombre n'est pas stocké dans la structure de données")
        
        self.__dir_lst[name-1] += 1
        
        if self.__dir_lst[name-1] > file_instance(name)+1:
            self.__dir_lst[name-1] = 2
            
        
        
        if name-1 == 0:
            color = "red"
        elif name-1 == 1:
            color = "purple"
        elif name-1 == 2:
            color = "blue"
            
        return "{}/{}{}.jpg".format(name, color, self.__dir_lst[name-1]-1)


def read_ascii(name):
    lst = []
    with open(name, "r") as file:
        for line in file:
            line = line.strip("\n")
            line = list(line)
            
            if lst == []:
                nbr_line = len(line)
            else:
                if nbr_line != len(line):
                    raise Exception("Toutes les lignes n'ont pas la même longueur")
                
            lst.append(line)
    return lst

def tool():
    try:
        width = int(input("Choisissez la longueur des images: "))
        height = int(input("Choisissez la hauteur des images: "))
        size = (width, height)
    except:
        raise Exception("L'élément entré n'est pas un nombre entier...")
    
    filename = input("Choisissez le nom du fichier à charger: ")
    try:
        lst_file = read_ascii(filename)
    except:
        raise FileNotFoundError("Le fichier entré n'existe pas...")
    lenght_width = len(lst_file[0])
    lenght_height = len(lst_file)
    
    path = input("Choisissez le dossier où se trouve les images d'origines: ")
    
    imf = ImageFolder(path)
    
    final_im = Image.new("RGBA", (width*lenght_width, height*lenght_height))
    
    
    counter_height = 0
    for a in range(len(lst_file)):
        counter_width = 0
        counter_height += 1
        temp_lst_file = lst_file[a]
        for b in range(len(temp_lst_file)):
            counter_width += 1
            file_im = imf.next(temp_lst_file[b])
            if file_im != "None":
                path_file = path + file_im
                im = Image.open(path_file)
                im = im.resize(size)
                
            else:
                im = Image.new("RGBA", size)
            
            box_crop = (0, 0, width, height)
            region_crop = im.crop(box_crop)
            
            final_box_crop = (width*(counter_width-1), height*(counter_height-1), width*counter_width, height*counter_height)
            
            final_im.paste(region_crop, final_box_crop)
            
    final_im.show()
    
if __name__ == "__main__":
    tool()

