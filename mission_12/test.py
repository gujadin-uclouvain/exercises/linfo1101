import mission12 as m
import unittest

class ImageFolderTest(unittest.TestCase):
    
    def test_next(self):
        obj = m.ImageFolder("./")
        name = "1"
        result = obj.next(name)
        self.assertEqual("1/red1.jpg", result)

def test(answer):
    import sys
    ligne = sys._getframe(1).f_lineno
    if answer:
        msg = "Test de la ligne {0} => OK.".format(ligne)
    else:
        msg = ("Test de la ligne {0} => ERREUR.".format(ligne))
    print(msg)
    
def test_read_ascii():
    test(m.read_ascii("smiley.txt") == [[' ', ' ', ' ', '1', '1', '1', '1', '1', '1', '1', '1', '1', ' ', ' ', ' '], [' ', ' ', '1', '3', '3', '3', '3', '3', '3', '3', '3', '3', '1', ' ', ' '], [' ', '1', '3', '3', '1', '1', '3', '3', '3', '1', '1', '3', '3', '1', ' '], [' ', '1', '3', '3', '3', '3', '3', '3', '3', '3', '3', '3', '3', '1', ' '], [' ', '1', '3', '1', '3', '3', '3', '3', '3', '3', '3', '1', '3', '1', ' '], [' ', '1', '3', '3', '1', '1', '1', '1', '1', '1', '1', '3', '3', '1', ' '], [' ', ' ', '1', '3', '3', '3', '3', '3', '3', '3', '3', '3', '1', ' ', ' '], [' ', ' ', ' ', '1', '1', '1', '1', '1', '1', '1', '1', '1', ' ', ' ', ' ']])
    test(m.read_ascii("smiley.txt") != ['2', '1', '3'])
    
if __name__ == "__main__":
    test_read_ascii()
    unittest.main()
