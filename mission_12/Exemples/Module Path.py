import os

def files(path):
    lst = []
    with os.scandir(path) as it:
        for entry in it:
            if not entry.name.startswith('.') and entry.is_file():
                lst.append(entry.name)
    return lst

def directories(path):
    lst = []
    with os.scandir(path) as it:
        for entry in it:
            if entry.is_dir():
                lst.append(entry.name)
                
    return lst

def subfiles(dir):
    lst = []
    with os.scandir(dir) as it:
        for entry in it:
            if not entry.name.startswith('.') and entry.is_file():
                lst.append(os.path.join(dir, entry.name))
            
    return lst

print(subfiles("/home/guillaume/Desktop/SINFBAC1/LINFO1101 - Intro Programmation/Mission 12/"))