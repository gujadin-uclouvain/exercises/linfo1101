Lors de cette septième mission, nous avons été chargé de créer un outil gérant le traitement de fichier avec des dictionnaires.

Fonctionnement du programme:

	- Il est divisé en plusieurs parties:

		- Le fichier "search.py" qui contient toutes les fonctions du programme.

		- Le fichier "test.py" qui, comme son nom l'indique, test si les fonctions du fichier "search.py" fonctionnent.

		- Les différents fichiers textes (Les dialogues d'un film et d'un jeu connu) pour les tests.

	- Le fichier "search.py":
 
	    - readfile(filename)
	    
	    - get_words(line)
	    
	    - occurrences(line, word)
	    
	    - create_index(filename)
	    
	    - get_lines(words, index)
	    
	    - L'outil utilisant les fonctions ci-dessus

Problèmes:

	- A cause d'un manque de temps, nous n'avons pas eu le temps de faire tout les tests pour toutes les fonctions.