#Jadin Guillaume
import sys
###SECTION FONCTIONS -- BEGIN###
def readfile(filename):
    """
    Retourne une liste des lignes (avec tout les caractères) d'un fichier "filename"
    
    pre: filename est un fichier existant
    post: retourne une liste des lignes appartenant au fichier "filename"
    """
    try :
        with open(filename, "r") as file:
            lst = []
            for line in file:
                lst.append(line)
            return lst
    except :
        print ("No such file or directory")

def get_words(line):
    """
    Retourne une liste des mots dans une chaîne de caractères donnée, en minuscules, et sans ponctuation.
    """
    lst = []
    for words in line.strip(" .,/[({})];.:!?'\n\t").split():  
        lst.append(words.strip(" .,/[({})];.:!?'\n\t").lower())
    return lst

def occurrences(line, word):
    """
    Compte les nombre de fois qu'un mot est dans une ligne.
    """
    count = 0
    for g in line:
        if g == word:
            count += 1
    return count

def create_index(filename):
    """
    Crée un index pour un fichier donné.
    pre: filename est un fichier existant
    post: Crée un 'index' pour le fichier avec nom 'filename'
    """
    with open (filename, "r") as file:
        lst = []
        d = {}
        for line in file:
            lst.append(get_words(line))        
        for (key, value) in enumerate(lst):
            for g in range(len(value)):
                if value[g] not in d:
                    d[value[g]] = {}
                d[value[g]][key] = occurrences(value, value[g])
        return d

def get_lines(words, index):
    """
    Retourne les identifiants des lignes qui contiennent tous les mots spécifiés
    dans la liste "words", en utilisant le dictionnaire contenant l'index "index".
    
    pre: words est une liste et index un dictionnaire
    post: retourne les identifiants des lignes
    """
    lst = []
    for word in words: #Cherche les mots un par un
        if word in index: 
            for key in list(index[word].keys()): # Cherche les 'key' pour chaques mots
                if key not in lst:
                    lst.append(key)
                    
    if lst == []:
        return "These words aren't in the file"
    else:
        return lst
###SECTION FONCTIONS -- END###

sep = "-"*82
while True:
    try:
        filename = input("Choose a filename (with extension [.txt, .dat, .conf, .ini, ...]) => ")
        while True:
            print(sep)
            words = input("Type a word or a list of words => ")
            words = words.lower().strip(" .,/[({})];.:!?'\n\t").split()
            print(get_lines(words, create_index(filename)))
            print(sep)
            try_other = input("Would you want to enter another word(s) (Y/N) ? => ").upper()
            if try_other != "N":
                pass
            else:
                print(sep)
                filename_other = input("Would you want to change the filename (Y/N) ? => ").upper()
                if filename_other != "N":
                    break
                else:
                    print(sep)
                    exit = input("Would you want to exit the program (Y/N) ? => ").upper()
                    if exit != "N":
                        sys.exit()
                    else:
                        pass
    except FileNotFoundError:
        print(sep)
        print("No such file or directory")
        print(sep)
    except SystemExit:
        print(sep)
        print("\tGoodbye User")
        print(sep)
        sys.exit()
    except:
        print(sep)
        print("The program has encountered an unexpected error")
        print(sep)