###
class Node:
    
    def __init__(self, value = None, next = None):
        self.__value = value
        self.__next = next
        
    def get_value(self):
        return self.__value
    
    def get_next(self):
        return self.__next
    
class LinkedList:
    
    def __init__(self):
        self.__size = 0
        self.__head = None
        
    def get_size(self):
        return self.__size
    
    def get_head(self):
        return self.__head
    
    def add(self, data):
        node = Node(data, self.get_head())
        self.__head = node
        self.__size += 1
        
    def get_reverse(self):
        data = ""
        while self.get_head() != None:
            previous = self.get_head()
            data += previous.get_value()
            self.__head = self.get_head().get_next()
        return data
    
l = LinkedList()
l.add("b")
l.add("f")
l.add("v")
l.add("g")
l.add("E")
l.add("n")
l.add("F")
l.add("0")
l.add("8")
l.add("b")
l.add("Z")
l.add("6")
l.add("l")
l.add("Q")
print(l.get_reverse())