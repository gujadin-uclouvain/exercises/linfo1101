#Programme adapté par Jadin Guillaume et Ndayisaba Pascale
import turtle                # module des graphiques tortue
tortue = turtle.Turtle()     # créer une nouvelle tortue
tortue.speed("fastest")      # tracé rapide

def square(size, color):
    """Trace un carré plein de taille `size` et de couleur `color`.

    pre: `color` spécifie une couleur.
         La tortue `tortue` est initialisée.
         La tortue est placée à un sommet et orientée en direction d'un
         côté du carré.
    post: Le carré a été tracé sur la droite du premier côté.
          La tortue est à la même position et orientation qu'au départ.
    """
    tortue.color(color)
    tortue.pendown()
    tortue.begin_fill()
    for i in range(4):
        tortue.forward(size)
        tortue.right(90)
    tortue.end_fill()
    tortue.penup()
    
def rectangle_settings(width, height):
    "Règle les problème d'affichage d'un drapeau en reportant la tortue en haut à gauche."
    tortue.left(90)
    tortue.forward(height//2 - 10)
    tortue.left(90)
    tortue.forward(width//2)
    tortue.left(180)
    
def rectangle(width, height, color):
    """Trace un rectangle plein de dimensions "width" x "height" et de couleur "color".

    pre: "color" spécifie une couleur.
         La tortue "tortue" est initialisée.
         La tortue est placée à un sommet et orientée en direction d'un
         côté du rectangle.
    post: Le rectangle a été tracé sur la droite du premier côté.
          La tortue est à la même position et orientation qu'au départ.
    """
    tortue.color(color)
    tortue.pendown()
    rectangle_settings(width, height)
    tortue.begin_fill()
    for i in range(2):
        tortue.forward(width)
        tortue.right(90)
        tortue.forward(height)
        tortue.right(90)
    tortue.end_fill()
    tortue.penup()
    
def rectangle_horizontal(width, height, color):
    "Crée un rectangle horizontal utilisé pour les drapeaux à trois bandes horizontales."
    tortue.color(color)
    tortue.pendown()
    tortue.begin_fill()
    tortue.forward(width)
    tortue.right(90)
    tortue.forward(height)
    tortue.right(90)
    tortue.forward(width)
    tortue.right(180)
    tortue.end_fill()
    tortue.penup()
    
def rectangle_vertical(width, height, color):
    "Crée un rectangle vertical utilisé pour les drapeaux à trois bandes verticales."
    tortue.color(color)
    tortue.pendown()
    tortue.begin_fill()
    tortue.forward(width//3)
    tortue.right(90)
    tortue.forward(width*2//3)
    tortue.right(90)
    tortue.forward(width//3)
    tortue.right(90)
    tortue.forward(width*2//3)
    tortue.right(90)
    tortue.forward(width//3)
    tortue.end_fill()
    tortue.penup()
    
def star(size):
    "Dessine une étoile jaune"
    tortue.color("#FFCC00")
    tortue.pendown()
    tortue.begin_fill()
    for i in range(5):
        tortue.right(72)
        tortue.forward(size)
        tortue.left(72)
        tortue.forward(size)
        tortue.right(72)
    tortue.end_fill()
    tortue.penup()

def round_star(size):
    "Dessine une série de douzes étoiles en cercle"
    r = 0
    for i in range(12):
        tortue.right(r)
        r = r + 30
        tortue.penup()
        tortue.forward(100)
        tortue.setheading(0)
        tortue.pendown()
        star(size)
        tortue.home()
        tortue.setheading(0)
        
def european_flag(width):
    "Permet de dessiner le drapeau bleu de l'union européenne"
    rectangle(width*3//2, width, "#003399")
    tortue.home()
    round_star(8)
    tortue.left(90)
    tortue.forward(100)
    tortue.left(90)
    tortue.forward(200)
    tortue.setheading(0)

def three_color_flag(width, color1, color2, color3):
    "Permet de dessiner les drapeaux à 3 bandes horizontales"
    for color in [color1, color2, color3]:
        rectangle_horizontal(width, width*2//3//3, color)
    tortue.left(90)
    tortue.forward(width*2//3)
    tortue.right(90)
    tortue.forward(width+10)
        
def belgian_flag(width):
    "Dessine le drapeau belge"
    for color in ["black", "#FDDA24", "#EF3340"]:
        rectangle_vertical(width//3, width*2//3, color)
        
def french_flag(width):
    "Dessine le drapeau français"
    for color in ["#0055A4", "white", "#EF4135"]:
        rectangle_vertical(width//3, width*2//3, color)
        

european_flag(300)
tortue.goto(-215, 250)
three_color_flag(100, "#AE1C28", "white", "#21468B") #Dutch Flag
three_color_flag(100, "black", "#FF0000", "#FFCC00") #German Flag
three_color_flag(100, "#EF3340", "white", "#00A3E0") #Luxemburg Flag
french_flag(300)
tortue.goto(-215, -200)
for f in range(4):
    belgian_flag(300)
    tortue.forward(10)