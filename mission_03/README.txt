#Programme adapté par Jadin Guillaume et Ndayisaba Pascale

Pour cette troisième mission, nous avons réalisé , grâce au module "turtle"; différents drapeaux allant de celui de l'Union Européenne jusqu'à celui de la Belgique, France, Allemagne, Luxembourg et des Pays-Bas.

Le programme fonctionne de la manière suivante:

D'abord, on initialise le module "turtle" et on crée une nouvelle tortue.

Ensuite, nous avons appliqué une série de fonctions pour nous facilité la vie comme par exemple:
	- Une qui crée le drapeau de L'UE,
	- Une qui crée le drapeau français et belge
	- Une qui crée des drapeaux avec trois bandes horizontales
	- ...

Enfin, ces fonctions ont été appliquées avec des commandes permettant de mettre en page les différents drapeaux.

REM: Nous avons rencontré des difficultés avec la mis en circle des 12 étoiles de l'UE mais cela à pût être réglé.