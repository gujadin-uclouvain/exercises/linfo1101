import turtle                # module des graphiques tortue
tortue = turtle.Turtle()     # créer une nouvelle tortue
tortue.speed("fastest")      # tracé rapide

def rectangle_horizontal(width, height, color):
    tortue.color(color)
    tortue.pendown()
    tortue.begin_fill()
    tortue.forward(width)
    tortue.right(90)
    tortue.forward(height)
    tortue.right(90)
    tortue.forward(width)
    tortue.right(180)
    tortue.end_fill()
    tortue.penup()
    
def rectangle_vertical(width, height, color):
    tortue.color(color)
    tortue.pendown()
    tortue.begin_fill()
    tortue.forward(width)
    tortue.right(90)
    tortue.forward(height)
    tortue.right(90)
    tortue.forward(width)
    tortue.right(90)
    tortue.forward(height)
    tortue.right(90)
    tortue.forward(width)
    tortue.end_fill()
    tortue.penup()
    
def three_color_flag(width, color1, color2, color3):
    for color in [color1, color2, color3]:
        rectangle_horizontal(width, width*2//3//3, color)
        
def belgian_flag(width):
    for color in ["black", "yellow", "red"]:
        rectangle_vertical(width//3, width*2//3, color)
        
three_color_flag(100, "red", "white", "cyan")
three_color_flag(100, "black", "red", "yellow")
three_color_flag(100, "red", "white", "blue")
belgian_flag(100)