import turtle                # module des graphiques tortue
tortue = turtle.Turtle()     # créer une nouvelle tortue
tortue.speed("fastest")      # tracé rapide

def rectangle(width, height, color):
    """Trace un rectangle plein de dimensions "width" x "height" et de couleur "color".

    pre: "color" spécifie une couleur.
         La tortue "tortue" est initialisée.
         La tortue est placée à un sommet et orientée en direction d'un
         côté du rectangle.
    post: Le rectangle a été tracé sur la droite du premier côté.
          La tortue est à la même position et orientation qu'au départ.
    """
    tortue.color(color)
    tortue.pendown()
    tortue.begin_fill()
    for i in range(2):
        tortue.forward(width)
        tortue.right(90)
        tortue.forward(height)
        tortue.right(90)
    tortue.end_fill()
    tortue.penup()

rectangle(300, 200, "blue")