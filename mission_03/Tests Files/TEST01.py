import turtle                # module des graphiques tortue
tortue = turtle.Turtle()     # créer une nouvelle tortue
tortue.speed(20)      # tracé rapide

def belgian_flag(width):
    for color in ["black", "yellow", "red"]:
        tortue.color(color)
        tortue.pendown()
        tortue.begin_fill()
        tortue.forward(width//3)
        tortue.right(90)
        tortue.forward(width*2//3)
        tortue.right(90)
        tortue.forward(width//3)
        tortue.right(90)
        tortue.forward(width*2//3)
        tortue.right(90)
        tortue.forward(width//3)
        tortue.end_fill()
        tortue.penup()

def french_flag(width):
    for color in ["blue", "white", "red"]:
        tortue.color(color)
        tortue.pendown()
        tortue.begin_fill()
        tortue.forward(width//3)
        tortue.right(90)
        tortue.forward(width*2//3)
        tortue.right(90)
        tortue.forward(width//3)
        tortue.right(90)
        tortue.forward(width*2//3)
        tortue.right(90)
        tortue.forward(width//3)
        tortue.end_fill()
        tortue.penup()
        
def luxemburg_flag(width):
    for color in ["red", "white", "blue"]:
        tortue.color(color)
        tortue.pendown()
        tortue.begin_fill()
        tortue.forward(width)
        tortue.right(90)
        tortue.forward((width*2//3)//3)
        tortue.right(90)
        tortue.forward(width)
        tortue.right(180)
        tortue.end_fill()
        tortue.penup()
        
belgian_flag(100)
luxemburg_flag(100)