import turtle                # module des graphiques tortue
tortue = turtle.Turtle()     # créer une nouvelle tortue
tortue.speed(10)      # tracé rapide

def rectangle_settings(width, height):
    tortue.left(90)
    tortue.forward(height//2 - 10)
    tortue.left(90)
    tortue.forward(width//2)
    tortue.left(180)
    
def rectangle(width, height, color):
    tortue.color(color)
    tortue.pendown()
    rectangle_settings(width, height)
    tortue.begin_fill()
    for i in range(2):
        tortue.forward(width)
        tortue.right(90)
        tortue.forward(height)
        tortue.right(90)
    tortue.end_fill()
    tortue.penup()
    
def star(size):
    tortue.color("yellow")
    tortue.pendown()
    tortue.begin_fill()
    for i in range(5):
        tortue.right(72)
        tortue.forward(size)
        tortue.left(72)
        tortue.forward(size)
        tortue.right(72)
    tortue.end_fill()
    tortue.penup()

def round_star(size):
    r = 0
    for i in range(12):
        tortue.right(r)
        r = r + 30
        tortue.penup()
        tortue.forward(100)
        tortue.setheading(0)
        tortue.pendown()
        star(size)
        tortue.home()
        tortue.setheading(0)
        
def european_flag(width):
    rectangle(width*3//2, width, "blue")
    tortue.home()
    round_star(8)
    tortue.left(90)
    tortue.forward(100)
    tortue.left(90)
    tortue.forward(200)
    tortue.setheading(0)
    

        
european_flag(300)