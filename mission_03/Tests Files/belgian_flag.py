import turtle                # module des graphiques tortue
tortue = turtle.Turtle()     # créer une nouvelle tortue
tortue.speed(0)      # tracé rapide

def belgian_flag(width):

    for color in ["black", "yellow", "red"]:
        tortue.color(color)
        tortue.pendown()
        tortue.begin_fill()
        tortue.forward(width//3)
        tortue.right(90)
        tortue.forward(width*2//3)
        tortue.right(90)
        tortue.forward(width//3)
        tortue.right(90)
        tortue.forward(width*2//3)
        tortue.right(90)
        tortue.forward(width//3)
        tortue.end_fill()
        tortue.penup()

belgian_flag(300)