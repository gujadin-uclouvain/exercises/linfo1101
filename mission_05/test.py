#Rédigé par Jadin Guillaume et Kimberly Sanchez
import sorted_belgian_communes as sbc

sep = "-"*60

def test(answer):
    import sys
    ligne = sys._getframe(1).f_lineno
    if answer:
        msg = "Test de la ligne {0} => OK.".format(ligne)
    else:
        msg = ("Test de la ligne {0} => ERREUR.".format(ligne))
    print(msg)
    
communes_verify_order_ERROR = [("Amay", (664097.4560020705, 5603092.769621753)),("Aalter", (531519.6775850406, 5659184.536941301)),\
                               ("Aarschot", (629867.1340910662, 5649141.00455739)),("Aartselaar", (596785.2232017588, 5665558.287847248)),\
                               ("Affligem", (578131.916279454, 5639292.55774853)),("Aiseau-Presles", (611605.8849598696, 5585675.111576218)),\
                               ("Alken", (661843.2732558982, 5637521.925917723)),("Alveringem", (476862.9305763073, 5647819.327231347)),\
                               ("Aalst", (575061.8368696974, 5644396.819551783))]
    
def test_verify_order():
    print("""Test de la fonction 'verify_order(communes)':
    """)
    test(sbc.verify_order(sbc.all_communes) == True)
    test(sbc.verify_order(communes_verify_order_ERROR) == False)
    
def test_coordinate():
    print("""Test de la fonction 'coordinate(commune,all_communes)':
    """)
    test(sbc.coordinate("Oostende", sbc.all_communes) == (495340.40303655533, 5672690.90358615))
    test(sbc.coordinate("Profondeville", sbc.all_communes) == (630756.4802517692, 5581942.712577484))
    test(sbc.coordinate("Waterloo", sbc.all_communes) != (613451.0085317963, 5596370.909098546))
    
def test_vol_oiseau():
    print("""Test de la fonction 'vol_oiseau(commune1, commune2, all_communes)':
    """)
    test(sbc.vol_oiseau("Zwijndrecht", "Zwalm", sbc.all_communes) == (43219.093409538735, 37029.41802968923))
    test(sbc.vol_oiseau("Zwijndrecht", "Zwalm", sbc.all_communes) != (74521.268992928735, 37029.41802968923))
    
def test_distance():
    print("""Test de la fonction 'distance(commune1, commune2, all_communes)':
    """)
    test(sbc.distance("Zingem", "Zoersel", sbc.all_communes) == (83624.72973411652))
    test(sbc.distance("Profondeville", "Oostende", sbc.all_communes) == (163011.49695558957))
    test(sbc.distance("Stekene", "Lille", sbc.all_communes) != (174252.9562822881))
    
communes_test = [("Profondeville", (630756.4802517692, 5581942.712577484)),\
    ("Putte", (614194.1850707538, 5656647.881175696)),("Puurs", (591653.1356574316, 5658979.364287738)),\
    ("Péruwelz", (539690.5109855694, 5597710.9640553575)),("Quaregnon", (559767.3072031111, 5588081.2795319455)),\
    ("Quivrain", (549362.7960988614, 5582102.289368963)),("Quévy", (566978.3875872826, 5579449.22775478)),\
    ("Raeren", (294432.258541117, 5620507.395167005)),("Ramillies", (633422.4564655169, 5613071.581426517)),\
    ("Ranst", (610638.082172616, 5672334.356148241))] 

def test_distances():
    print("""Test de la fonction 'distances(commune, all_communes)':
    """)
    test(sbc.distances("Profondeville", communes_test) == [(0.0, 'Profondeville'), (76519.09459050343, 'Putte'), (86392.80794829229, 'Puurs'), (92421.0393420346, 'Péruwelz'), (71254.08545751416, 'Quaregnon'), (81393.8405822928, 'Quivrain'), (63826.81701668111, 'Quévy'), (338528.01487100957, 'Raeren'), (31242.82165542345, 'Ramillies'), (92603.45117053756, 'Ranst')])
    test(sbc.distances("Profondeville", communes_test) != [(174252.9562822881, 'Profondeville'), (76519.09459050343, 'Putte'), (86392.80794829229, 'Puurs'), (92421.0393420346, 'Péruwelz'), (71254.08545751416, 'Quaregnon'), (81393.8405822928, 'Quivrain'), (63826.81701668111, 'Quévy'), (338528.01487100957, 'Raeren'), (31242.82165542345, 'Ramillies'), (92603.45117053756, 'Ranst')])
    
def test_closest():
    print("""Test de la fonction 'closest(commune, all_communes, k)':
    """)
    test(sbc.closest("Profondeville", communes_test, 4) == [(0.0, 'Profondeville'), (31242.82165542345, 'Ramillies'), (63826.81701668111, 'Quévy'), (71254.08545751416, 'Quaregnon')])
    test(sbc.closest("Profondeville", communes_test, 5) != [(0.0, 'Profondeville'), (63826.81701668111, 'Quévy'), (31242.82165542345, 'Ramillies'), (71254.08545751416, 'Quaregnon')])

def test_distance_matrix():
    pass
    
test_verify_order()
print(sep)
test_coordinate()
print(sep)
test_vol_oiseau()
print(sep)
test_distance()
print(sep)
test_distances()
print(sep)
test_closest()
print(sep)
test_distance_matrix()