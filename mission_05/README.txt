Lors de cette cinquième mission, nous avons été chargé de traiter une liste de communes (et leurs coordonnées) de la Belgique.

Fonctionnement du programme:

	- Il est divisé en deux parties:

		- La première est le fichier "sorted_belgian_communes.py" qui contient toutes les fonctions du programme.

		- La seconde est le fichier "test.py" qui, comme son nom l'indique, test si les fonctions du fichier "sorted_belgian_communes.py" fonctionnent.

	- Le fichier "sorted_belgian_communes.py" contient 7 fonctions:

		- "verify_order(communes)" 
			=> Vérifie que la liste de communes communes est triée par nom.

		- "coordinate(commune,all_communes)" 
			=> Trouve les coordonnées d'une commune dans la liste all_communes.

		- "vol_oiseau(commune1, commune2, all_communes)"
			=> Calcule la distance entre deux coordonnées (x1,y1) et (x2,y2).

		- "distance(commune, all_communes)" 
			=> Calcule la distance euclidienne entre deux communes dont les noms sont donnés.

		- "distances(commune, all_communes)"
			=> Calcule la distance euclidienne d'une commune vers toutes les autres communes de la Belgique.

		- "closest(commune all_communes, k)" 
			=> Calcule les k communes qui sont les plus proches d'une commune donnée commune.

		- "distance_matrix(communes,all_communes)"
			=> Calcule et retourne une matrice des distances entre les communes.

	- Le ficher "test.py" contient aussi 7 fonctions:

		- "test_verify_order(communes)"
		- "test_coordinate(commune,all_communes)" 
		- "test_vol_oiseau(commune1, commune2, all_communes)"
		- "test_distance(commune, all_communes)" 
		- "test_distances(commune, all_communes)"
		- "test_closest(commune all_communes, k)" 
		- "test_distance_matrix(communes,all_communes)"


		=> Ces septs fonctions testent respectivement les fonctions du fichier "sorted_belgian_communes.py".

Problèmes:

	- Nous n'avons malheureusement pas réussi à faire fonctionner la fonction distance_matrix à cause d'une erreur dans notre traitement de données. Le traitement des matrices est, de plus, un des éléments qui nous est le moins clair.
