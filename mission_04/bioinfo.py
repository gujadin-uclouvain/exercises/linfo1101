def is_adn(s):
    """
    Prend comme argument une chaîne de caractères s et retourne True
    si la chaîne de caractères contient uniquement les caractères a, t, c ou g
    (à la fois en majuscules et en minuscules) et False dans le cas contraire.
    
    
    Pre: Une chaîne de caractère s
    Post: Retourne True si la chaîne de caractères contient uniquement les caractères a, t, c ou g (à la fois en majuscules et en minuscules)
    et False dans le cas contraire.
    """
    for var in s:
        "ATCG".lower()
        if var not in "atcg":
            return False
    return True

def positions(s, p):
    """
    Pre: Deux chaînes de caractères s et p
    Post: Retourne les positions des occurences de p dans s
    """
    s_lo = s.lower()
    p_lo = p.lower()
    result = []
    index_p = 0
    for index_s, value_s in enumerate(s_lo):
        if value_s == p_lo[index_p]:
            if index_p < len(p_lo) - 1:
                index_p += 1
            else:
                result.append(index_s - index_p)
                index_p = 0
        else:
            index_p = 0
            if value_s == p_lo[index_p] and index_p < len(p) - 1:
                index_p += 1

    return result

def distance_h(s, p):
    """
    Pre: Deux Chaines de caractères s et p.
    Post: Calcule la distance de Hamming entre deux chaînes de caractères de longueurs égales. 
    """
    diff = 0
    if len(s) != len(p):
        return None
    for a, b in zip(s, p):
        if a != b:
            diff += 1
    return diff
        
def plus_long_palindrome(s):  
    """
    !!! Ne Fonctionne Pas !!!

    Pre: une chaine de caractère s.
    Post : Retourne une chaîne de caractères contenant le plus long palindrome de la chaîne passée en argument.
    """
    