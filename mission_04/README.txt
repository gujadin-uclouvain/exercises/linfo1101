Lors de cette quatrième mission, nous avons été chargé de traiter des données (des séquences d'ADN) et de les manipuler.

Fonctionnement du programme:

	- Il est divisé en deux parties:

		- La première est le fichier "bioinfo.py" qui contient toutes les fonctions du programme.

		- La seconde est le fichier "test.py" qui, comme son nom l'indique, test si les fonctions du fichier "bioinfo.py" fonctionnent.

	- Le fichier "bioinfo.py" contient 4 fonctions:

		- "is_adn(text)" 
			=> Vérifie si la séquence donnée est de l'ADN.

		- "positions(text,car)" 
			=> Retourne les positions des occurences des deux séquences.

		- "distance_h(text1,text2)" 
			=> Calcule la distance de Hamming entre deux chaînes de caractères de longueurs égales.

		- "plus_long_palindrome(text)" 
			=> Retourne une chaîne de caractères contenant le plus long palindrome de la chaîne passée en argument.

	- Le ficher "test.py" contient aussi 4 fonctions:

		- "test_is_adn()"
		- "test_positions()"
		- "test_distance_h()"
		- "test_plus_long_palindrome()"

		=> Ces quatres fonctions testent respectivement les fonctions du fichier "bioinfo.py".

Problèmes:

	- Nous n'avons malheureusement pas trouvé de solutions pour les fonctions "positions()" et "plus_long_palindrome()" car elles demandent de se déplacer dans une liste et il s'agit d'une partie du cours que nous ne comprennons, pour le moment, pas très bien. (Le manque de temps à été aussi la raison de ces deux manquements).